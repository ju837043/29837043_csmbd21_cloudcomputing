HOW TO USE

    1. Download all the python files and the Input data files.
    2. Open terminal
    2. Navigate to the directory where all the python files, input data files are located
    3. Type below to get the total number of flight from each airport
        $ python Read_Files.py | python Mapper_1.py | python Shuffle_Sort.py | python Reducer_1.py 
    4. Type below to get a passenger having had highest number of flights
        $ python Read_Files.py | python Mapper_2.py | python Shuffle_Sort.py | python Reducer_2.py 
    5. Output will be displayed in terminal. Also the output will be saved in the local directory
